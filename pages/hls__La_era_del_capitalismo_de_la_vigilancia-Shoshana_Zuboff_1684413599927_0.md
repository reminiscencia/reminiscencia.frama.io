file:: [La_era_del_capitalismo_de_la_vigilancia-Shoshana_Zuboff_1684413599927_0.pdf](../assets/La_era_del_capitalismo_de_la_vigilancia-Shoshana_Zuboff_1684413599927_0.pdf)
file-path:: ../assets/La_era_del_capitalismo_de_la_vigilancia-Shoshana_Zuboff_1684413599927_0.pdf

- ¿Qué es el Capitalismo de Vigilancia?
	- El capitalismo de la vigilancia reclama unilateralmente para sí la experiencia humana, entendiéndola como una materia prima gratuita que puede traducir en datos de comportamiento.
	  hl-page:: 12
	  ls-type:: annotation
	  id:: 646621f3-659b-4a7f-a958-d855f4090c34
	  hl-color:: yellow
	- Algunos de dichos datos se utilizan para mejorar productos o servicios, el resto es considerado como un **excedente conductual privativo** («propiedad») de las propias empresas capitalistas de la vigilancia
	- Se fabrican productos predictivos que prevén lo que cualquiera de ustedes hará ahora, en breve y más adelante.
- ls-type:: annotation
  hl-page:: 12
  hl-color:: yellow
  id:: 64662390-c2e2-4033-8ad3-7e9bdc2eacbb
- Estos productos predictivos son comprados y vendidos en un nuevo tipo de mercado de predicciones de comportamientos que yo denomino mercados de futuros conductuales
  ls-type:: annotation
  hl-page:: 12
  hl-color:: yellow
  id:: 64665c9d-b662-4321-99be-f45b87eb7186
- Con el tiempo, los capitalistas de la vigilancia descubrieron que los datos conductuales más predictivos se obtienen interviniendo en la marcha misma de las cosas para empujar a, persuadir de, afinar y estimular ciertos comportamientos a fin de dirigirlos hacia unos resultados rentables. Fueron las presiones competitivas las que produjeron este cambio: ahora los procesos automatizados llevados a cabo por máquinas no solo conocen nuestra conducta, sino que también moldean nuestros comportamientos en igual medida. **A partir de esa reorientación desde el conocimiento hacia el poder, ya no basta con automatizar los flujos de información referida a nosotros, el objetivo ahora es automatizarnos (a nosotros mismos)**. En esta fase de la evolución del capitalismo de la vigilancia, los medios de producción están supeditados a unos cada vez más complejos y exhaustivos «medios de modificación conductual». De ese modo, el capitalismo de la vigilancia da a luz a una nueva especie de poder que yo llamo instrumentarismo. El poder instrumentario conoce el comportamiento humano y le da forma, orientándolo hacia los fines de otros. En vez de desplegar armamentos y ejércitos, obra su voluntad a través del medio ambiente automatizado conformado por una arquitectura informática cada vez más ubicua de dispositivos «inteligentes», cosas y espacios conectados en red
  ls-type:: annotation
  hl-page:: 12
  hl-color:: red
  id:: 64be42cf-c148-4af5-833a-57f4f345774c