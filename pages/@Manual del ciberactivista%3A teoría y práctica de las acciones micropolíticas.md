tags:: [[Society]]
date:: 2015
publisher:: Bandaàparte
isbn:: 978-84-944086-0-1
title:: @Manual del ciberactivista: teoría y práctica de las acciones micropolíticas
item-type:: [[book]]
access-date:: 2023-06-15T01:50:49Z
rights:: http://creativecommons.org/licenses/by-nc-nd/4.0/
original-title:: Manual del ciberactivista: teoría y práctica de las acciones micropolíticas
language:: spa
url:: http://archive.org/details/ManualDelCiberactivista.TeoriaYPracticaDeLasAccionesMicropoliticas
short-title:: Manual del ciberactivista
authors:: [[Javier de la Cueva]]
library-catalog:: Internet Archive
links:: [Local library](zotero://select/groups/5065565/items/JUXNACVQ), [Web library](https://www.zotero.org/groups/5065565/items/JUXNACVQ)

- [[Abstract]]
	- Manual para entender qué son y qué no son las acciones micropolíticas y una guía con lo fundamental para llevar a cabo acciones micropolíticas en el entorno digital. El contenido de la obra se halla dividido en dos partes, una primera teórica y otra segunda práctica. En la primera parte se realiza una explicación analítica del ciberactivismo mientras que la segunda se centra en reflexiones sobre aspectos concretos que pudieran ser útiles para quien desee planificar alguna acción.
	  
	  This book has an editable web page on Open Library.