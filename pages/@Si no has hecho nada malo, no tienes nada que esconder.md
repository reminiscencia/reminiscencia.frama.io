date:: [[Monday, 11/01/2021]]
extra:: Page Version ID: 132314098
encyclopedia-title:: "Wikipedia, la enciclopedia libre"
title:: @Si no has hecho nada malo, no tienes nada que esconder
item-type:: [[encyclopediaArticle]]
access-date:: 2023-06-16T19:48:40Z
rights:: Creative Commons Attribution-ShareAlike License
original-title:: "Si no has hecho nada malo, no tienes nada que esconder"
language:: es
url:: "https://es.wikipedia.org/w/index.php?title=Si_no_has_hecho_nada_malo,_no_tienes_nada_que_esconder&oldid=132314098"
library-catalog:: Wikipedia
links:: [Local library](zotero://select/groups/5065565/items/TK22CH7G), [Web library](https://www.zotero.org/groups/5065565/items/TK22CH7G)

- [[Abstract]]
	- El argumento del nada que esconder es utilizado frecuentemente para afirmar que los programas de vigilancia no amenazan la privacidad a menos que descubran actividades ilegales. Y en caso de hacerlo, la persona cometiendo estas actividades no tiene el derecho de hacerlas en privado.
	  Una persona a favor de este argumento puede afirmar que "no tiene nada que esconder" y por lo tanto no expresa oposición a la vigilancia del gobierno. Un individuo puede decir que una persona no debería preocuparse por la vigilancia gubernamental si no tiene "nada que esconder".
	  El lema "Si no tienes nada que esconder, no tienes nada que temer" se ha usado en defensa del sistema de circuito cerrado de vigilancia utilizado en el Reino Unido.
- [[Attachments]]
	- [Snapshot](https://es.wikipedia.org/wiki/Si_no_has_hecho_nada_malo,_no_tienes_nada_que_esconder) {{zotero-imported-file CEQQ3PS8, "Si_no_has_hecho_nada_malo,_no_tienes_nada_que_esconder.html"}}