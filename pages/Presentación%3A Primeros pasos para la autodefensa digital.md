- # Primeros pasos para la autodefensa digital
- # Índice
  id:: 648a6531-a1b9-43b5-b95e-7ee32f34e2ea
	- **Conceptos previos**
	  + Contexto
	  + Seguridad poco a poco
	  + Los malos a.k.a modelo de amenazas
	  + Seguridad operacional vs. Seguridad intrumental
	  + Primeros pasos
	- **Estructura**
	  + Problema ![warning.png](../assets/warning_1686942075028_0.png)
	  + Preguntas ![question.png](../assets/question_1686942044135_0.png)
	  + Soluciones ![lightbulb.png](../assets/lightbulb_1686942086742_0.png)
	- **Seguridad básica**
	  + Contraseñas
	  + Software
	  + Dispositivos
	  + Bloqueo de móvil
	  + Cifrado de ordenador
	  + Conversaciones
	- **Privacidad y Anonimato**
	  + Recolección masiva
	  + Privacidad
	  + Redes
	  + Reconocimiento físico
	- **Seguridad Avanzada**
	  + Separación de perfiles
	  + No crear más información de la necesaria
	  + Pensamiento ofensivo
	  + OSINT
	  + Ingeniería social
# Conceptos previos
	- ## 0. Contexto
	  id:: 648d75aa-79bc-44aa-b728-5f7ecf53cb0b
	  + [[La tecnología es política]]
	  ![](https://telegra.ph/file/f510273db71478f2dc050.jpg){:align center, :height 574, :width 363}
	- ## 0. Contexto
	  id:: 648d75aa-f04f-4423-b932-ee8543270a6d
	  + La información es poder 
	  ![imagen-valor-datos.jpg](../assets/imagen-valor-datos_1686951162205_0.jpg){:height 427, :width 667,  :align center}
	- ## 0. Contexto
	  id:: 648d75aa-8594-49ec-8fe8-7fb7a040b95d
	  + No me importa la privacidad. 
	  + "[[No tengo nada que ocultar]]". 
	  ![image.png](../assets/image_1686944016946_0.png){:align center, :height 316, :width 430}
	- ## 0. Contexto
	  id:: 648d75aa-fce6-4758-9689-701413d72f28
	  + ¿Tendrías problemas en instalar una webcam en tu baño o en tu dormitorio?
	  + ¿Conoces todas y cada una de las leyes tan bien como para poder decir que no haces nada ilegal?
	  + El haber tenido determinadas enfermedades, o simplemente algunos hábitos "no saludables" te pueden hacer inasegurable en paises como EE.UU.
	- ## 0. Contexto
	  id:: 648d7831-5bef-4fd2-a5a7-d5103f89c244
	  + "Nada que ocultar... mientras estés 100% de acuerdo con la visión y las políticas de tu gobierno". *Emily Kate Goulding*
	  + "Decir que no importa el derecho a la privacidad porque no tienes nada que ocultar es como justificar que no importa la libertad de expresión porque no tienes nada que decir". *Edward Snowden*
	  + "Si no tenemos nada que ocultar, ¿por qué estamos bajo vigilancia?." *Jake Lawler*
	  ![https://i.blogs.es/f67394/privacyplease/1366_2000.jpg](https://i.blogs.es/f67394/privacyplease/1366_2000.jpg){:height 173, :width 267, :align center}
	- ## 0. Contexto
	  id:: 648d75aa-47c4-4efe-ace5-bfbca49ef05d
	  + [[Capitalismo de Vigilancia]]
	  ![image.png](../assets/image_1686190705183_0.png){:height 190, :width 616, :align center}
	- ## 0. Contexto
	  id:: 648d75aa-f13a-40c9-81ed-6b6a6d8cdf23
	  + Modelos predictivos de publicidad y comportamiento futuro 
	  ![Uso de datos de comportamiento para modelos predictivos de publicidad](https://www.alejandrobarros.com/wp-content/uploads/2021/02/discovery-of-behavioral-surplus.png){:height 490, :width 296, :align center}
	- ## 0. Contexto
	  + Escándalo de Cambridge Analytica
	  ![https://i0.wp.com/kokomansion.com/wp-content/uploads/2018/04/Cambridge-Analytica.jpg?fit=700%2C428&ssl=1](https://i0.wp.com/kokomansion.com/wp-content/uploads/2018/04/Cambridge-Analytica.jpg?fit=700%2C428&ssl=1){:height 317, :width 506, :align center}
	- ## 0. Contexto
	  + Publicaciones de Snowden y programas PRISM, XKEYSCORE o TEMPORA
	  ![CIA.jpg](../assets/CIA_1687016701674_0.jpg){:height 273, :width 246}
	- ## 0. Contexto
	  id:: 648d75aa-9f73-4e43-86e2-5c5d0d8848a5
	  + [[Efecto panóptico]]
	      + Sentir que nuestros actos estén bajo inspección, se rechazan pensamientos o actos subversivos. Revolución en el comportamiento humano para que no se cuestionen ciertos paradigmas establecidos por convención social.
	  ![efecto panóptico](https://i.pinimg.com/originals/09/9d/72/099d7246092b0171bf6a1f2ba7db9664.png){:height 215, :width 323, :align center}
	- ## 0. Contexto
	  id:: 648d75aa-5de9-4d52-9ade-8a9707a5eaca
	  + [[Soberanía tecnológica]]
	  ![https://www.faimaison.net/files/images/chatons-leprette-mai2019/Peha-Banquet-Degooglisons-CC-By.png](https://www.faimaison.net/files/images/chatons-leprette-mai2019/Peha-Banquet-Degooglisons-CC-By.png){:height 355, :width 468}
	- ## 1. Seguridad poco a poco:
	  id:: 648d75aa-c156-48f8-95b6-68eafa289090
	  * **No hay que asustarse ni agobiarse**.
	  * Los **cambios** se deben hacer **poco a poco** para no agotarse.
	  * [[Rutina de combate e interiorización de la seguridad]] 
	  ![image.png](../assets/image_1681909752230_0.png){:height 324, :width 428, :align center}
	- ## 2. Los malos a.k.a modelo de amenaza
	  id:: 648d75aa-f6e7-4c7a-bac7-0f6b7c75addf
	  + ¿Qué quieres proteger?
	  + Identificación y evaluación de riesgos
	- ## 2. Los malos a.k.a modelo de amenaza
	  id:: 648d75aa-0db7-496a-9f0f-809e2a115c59
	  + ¿De quién quieres defenderte?
	  ![https://www.no-gods-no-masters.com/images_designs/keep-your-city-clean-destroy-fascism-anti-racist-d001039460245.jpg](https://www.no-gods-no-masters.com/images_designs/keep-your-city-clean-destroy-fascism-anti-racist-d001039460245.jpg){:height 147, :width 141}
	- ## 3. Seguridad operacional vs. Seguridad intrumental
	  id:: 648d75aa-8c87-4e62-9a50-ef1381b60a21
	  +  ^^Seguridad operacional (OPSEC)^^: se centra en el uso de **procesos y protocolos para aumentar la seguridad**. 
	  + ^^Seguridad instrumental^^: se centra en el uso de **herramientas y aplicaciones para aumentar la seguridad**.
	- ## 4. Primeros pasos
	  id:: 648d75aa-883b-4b16-be0f-c21e673a289b
	  + **Evaluación de riesgos** 
	  + Análisis de los procesos y vectores de ataque actuales
	  + Proteger el **punto más débil**
	  + Elabora una **rutina** para empezar poco a poco
	  + Inventario de la **mochila de datos**
	  + Aligerar peso
	  + **Separar perfiles**
# Seguridad básica
	- ## 1. **Contraseñas**:  ![warning.png](../assets/warning_1686942075028_0.png)
	  
	  La mayoría de la seguridad depende de las contraseñas, pero todavía existen contraseñas como:
	  ```
	  passwordf
	  pepe1997
	  urss1917
	  Tr0ub4dor&3
	  ```
	  ¿Por qué se consideran inseguras estas contraseñas?
	- ## 1. **Contraseñas**:  ![warning.png](../assets/warning_1686942075028_0.png)
	  ![contraseñas.jpg](../assets/contraseñas_1686834811793_0.jpg){:height 443, :width 531}
	- ## 1. **Contraseñas**:  ![warning.png](../assets/warning_1686942075028_0.png)
	  Además sigue habiendo filtraciones de datos, exponiendo hasta las contraseñas seguras.
	  ![https://cdn.le-vpn.com/es/wp-content/uploads/2016/06/databreachinf1200x628v1-es-1.jpg](https://cdn.le-vpn.com/es/wp-content/uploads/2016/06/databreachinf1200x628v1-es-1.jpg){:height 348, :width 641, align center}
	- ## 1. **Contraseñas**: ![question.png](../assets/question_1686942044135_0.png)
	  + ¿Cuándo una contraseña es segura?
	  + ¿Cómo gestionamos las contraseñas?
	  + ¿Qué tipo de [[autenticación]] es más segura?
	- ## 1. **Contraseñas**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png)  
	  ^^**Seguridad Operacional**^^
	  + **Larga** es mejor que compleja
	  + **Cambiarlas** de vez en cuando
	  + **Nunca reutilizarlas**
	  + Es mejor **no tener que recordarlas**
	  + **No poner todos los huevos en la misma cesta**
	  + **Usar [[autenticación]] multifactor**
	- ## 1. **Contraseñas**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) 
	  ^^**Seguridad Operacional**^^
	  + Combinar varias **palabras aleatorias** que nos sean fáciles de recordar
	  + Activar la **verificación en dos pasos** en todos los servicios que lo permitan
	- ## 1. **Contraseñas**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) 
	  ^^**Seguridad Instrumental**^^
	  + [**HaveIBeenPwned**](https://haveibeenpwned.com/): sitio web para saber si algún servicio que usamos ha sido hackeado
	  + **Gestores de contraseñas**:
	      + [**KeePassXC**](https://keepassxc.org/): en local, móvil y con extensión para el navegador
	      + **[BitWarden](https://bitwarden.com/)**: servicio online
	      + **[VaultWarden](https://vaultwarden.us/)**: servicio para autoalojar
	- ## 2. **Software**: ![warning.png](../assets/warning_1686942075028_0.png)
	  + Aparecen nuevas vulnerabilidades todos los meses
	  + Aparece nuevo **malware frecuentemente**
	  + Descargamos e instalamos aplicaciones sin saber qué hacen realmente y cuál es su fuente
	  + Tu **móvil** ya no tiene mantenimiento y **deja de actualizarse**
	- ## 2. **Software**: ![question.png](../assets/question_1686942044135_0.png)
	  + ¿Cuándo se considera que el **software es seguro**?
	  + ¿Cuál es el riesgo de que una aplicación no sea segura? ¿Qué consecuencias puede tener?
	  + ¿El código libre más seguro que el código privativo? ¿GNU/Linux es más seguro que Windows?
	  + ¿Cómo nos protogemos frente al **acceso remoto**?
	- ### 2.1. **Software**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) 
	  ^^**Seguridad Operacional**^^
	  + **Actualizar frecuentemente**
	  + Instalar solo de **fuentes fiables**
	  + No instalar aplicaciones y olvidarnos de ellas
	  + Confiar en las **comunidades activas**
	  + **Compartir** las preocupaciones y **pensar en colectivo**
	- ### 2.2 **Software**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) 
	  ^^**Seguridad Operacional**^^
	  + Anti-Virus
	     + ClamAV como antivirus y UFW como firewall para GNU/Linux
	     + WindowsDefender es suficientemente bueno y viene por defecto en Windows
	- ## 3. **Dispositivos**: ![warning.png](../assets/warning_1686942075028_0.png)
	  + Los teléfonos móviles, mediante sus antenas de telefonía anuncian su posición constantemente.
	  + WiFi y Bluetooth también pueden (menos común y permite menos rango)
	  + Eso se usa para rastrear e identificar a activistas
	- ## 3. **Dispositivos**: ![warning.png](../assets/warning_1686942075028_0.png)
	  ![](https://ssd.eff.org/files/ssd/wysiwyg/pictures/553/content_endtoendencryptionmetadata-nolocks.png)
	- ## 3. **Dispositivos**: ![question.png](../assets/question_1686942044135_0.png)
	  	* ¿Cómo se puede rastrear un teléfono móvil?
	  	* ¿Para qué puede servir conocer las actividades históricas de las personas (participación en eventos o  relaciones personales)?
	- ### 3.1 **Dispositivos**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png)
	  id:: 648d75aa-1160-4b80-8dd7-657a1e9ebec7
	  ^^**Seguridad Operacional**^^
	  + Poner el **modo avión o apagar** cuando no se esté usando
	  + Activar opción de **apagado automático de WiFi y Bluetooth** si no está conectado
	  + Tener en cuenta qué información ofreces en una protesta
	- ### 3.1 **Dispositivos**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png)
	  ^^**Seguridad Instrumental**^^
	  + Modo avión
	  + Bolsa de Faraday
	- ## 4. **Bloqueo de móvil**: ![warning.png](../assets/warning_1686942075028_0.png)
	  + Dejas la huella del patrón marcado en la pantalla.
	  + Pierdes el móvil por la calle.
	  + Te arrestan en una protesta. La policía tiene tu móvil.
	  + **¿Qué información podrían obtener?**
	- ## 4. **Bloqueo de móvil**: ![question.png](../assets/question_1686942044135_0.png)
	  id:: 648d75aa-fec7-4aad-a7b5-d80e83495cb7
	  + Los **teléfonos móviles no fueron diseñados para la privacidad y seguridad** ¿Qué información guardamos en ellos?
	  + ¿Qué tiene **mayor valor**: el móvil o los datos que almacenas?
	- ## 4. **Bloqueo de móvil**: ![question.png](../assets/question_1686942044135_0.png)
	  + ¿Puede mi adversario **observarme desbloquear el teléfono** antes de ganar acceso físico a él?
	  + ¿Puede mi adversario **obligarme o coaccionarme para desbloquear el dispositivo**?
	  + ¿Mi adversario puede **recoger muestras biométricas** (huellas, iris, fotografías) para desbloquear el dispositivo con ellas?
	  + ¿Mi adversario cuenta con conocimientos o medios especializados para el **desbloqueo por fuerza bruta** de mi dispositivo?
	- ## 4. **Bloqueo de móvil**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) 
	  ^^**Seguridad Operacional**^^
	  + **Cifrar el móvil**
	  + Utilizar un **bloqueo seguro**
	  + **Separar perfiles**
	  + **Conoce al enemigo**
	- ## 4. **Bloqueo de móvil**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png)
	  id:: 648d75aa-b139-4755-9494-577559bb2dad
	  ^^**Seguridad Instrumental**^^
	  + No **utilizar (o desactivar) el bloqueo biométrico** antes de una protesta
	  + **Cifrado** nativo de Android o Apple
	  + [Eliminar aplicaciones instaladas de fábrica](https://github.com/0x192/universal-android-debloater)
	  + Sistemas operativos enfocados en privacidad: [[LineageOS]], [[crDroid]], [[DivestOS]], ... 
	  + Hardenizar Android: [GrapheneOS](https://grapheneos.org/), [CalyxOS](https://calyxos.org/) o [CopperheadOS](https://copperhead.co/android/)
	- ## 5. **Cifrado de ordenador**: ![warning.png](../assets/warning_1686942075028_0.png)
	  + Si se tiene acceso físico al ordenador estás vendido. 
	  + Estás de cervezas y te roban la mochila con el ordenador.
	  + Si la policía te arresta tendrá acceso físico.
	- ## 5. **Cifrado de ordenador**: ![question.png](../assets/question_1686942044135_0.png)
	  + ¿Qué **datos no tenemos cifrados**? ¿Pueden comprometernos a nosotros o a nuestro colectivo?
	  + ¿Qué tiene **mayor valor**: el ordenador o los datos que almacenas? ¿Copias de seguridad cifradas?
	- ## 5. **Cifrado de ordenador**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png)
	  ^^**Seguridad Operacional**^^
	  + Mantener el **ordenador apagado** cuando no se usa
	  + **Contraseña segura y secreta**
	- ^^**Seguridad Instrumental**^^
	  + Linux: [LUKS](https://wiki.archlinux.org/index.php/Dm-crypt) y [más en la ArchWiki](https://wiki.archlinux.org/index.php/Disk_encryption)
	  + Windows: [BitLocker](https://docs.microsoft.com/en-us/windows/security/information-protection/bitlocker/bitlocker-overview)
	  + Apple: [FileVault](https://support.apple.com/en-us/HT204837)
	- ## 6. **Conversaciones**: ![warning.png](../assets/warning_1686942075028_0.png)
	  id:: 648d88b9-f3fc-4779-b35d-1d5270089b49
	  + Para coordinar acciones en grupo es **necesario comunicarse**
	  + Siempre hay alguna persona que **habla más de la cuenta**
	  + A la policía le gusta saber lo que se dice en esos grupos
	- ## 6. **Conversaciones**: ![question.png](../assets/question_1686942044135_0.png)
	  + ¿Qué **temas sensibles** se hablan a través de **grupos abiertos**?
	  + ¿Existe un **anillo de confianza** entre las personas del grupo?
	  + ¿Qué persona suele **hablar más de la cuenta** de los asustos del colectivo?
	- ## 6. **Conversaciones**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) :
	  ^^**Seguridad Operacional**^^
	  + Cuidado con **quién entra en el grupo**
	  + **Borra los mensajes sensibles** o usa chats con autodestrucción
	  + Usar **protocolos interoperables y abiertos** para comunicarnos
	- ## 6. **Conversaciones**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) :
	  ^^**Seguridad Operacional**^^
	  + El **punto más débil** no eres tú, puede ser otra persona
	  + La seguridad en los grupos es el resultado de la **suma de compromisos individuales** por no exponer la privacidad de los demás miembros
	- ## 6. **Conversaciones**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) :
	  ^^**Seguridad Instrumental**^^
	  + [Signal](https://www.signal.org/)
	  + Conversations (jabber/xmpp)
	  + Element (matrix)
	  + Jami
	  + [Briar](https://briarproject.org/): mensajería cifrada de punto a punto sin necesidad de acceso a Internet. Usa bluetooth, WiFi y Tor
	  + aTox: protocolo Tox
# **Privacidad y Anonimato**
	- ## 1. **Recolección masiva** ![warning.png](../assets/warning_1686942075028_0.png)
	  + Nuevas formas de **recolección de datos == nuevas amenazas**. 
	  + Muchas veces, lo importante no es el contenido sino los **[[metadatos]]**.
	  + Los metadatos de una comunicación son **más informativos que el contenido** de la misma y a su **automatización es sencilla**. Los **metadatos revelan la identidad, costumbres y asociaciones de las personas**.
	- ## 2. **Privacidad** ![warning.png](../assets/warning_1686942075028_0.png)
	  + Con el **control y recolección de información** por parte de anunciantes, Google, Facebook, data brokers y prácticamente cualquiera, nuestra actividad en internet funciona para crear perfiles que **nos conocen mejor que nosotras mismas**.
	  ![cookies.jpg](../assets/cookies_1686963913306_0.jpg){:height 325, :width 306}
	- ## 2. **Privacidad**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) :
	  ^^**Seguridad Operacional**^^
	  + **Conoce el valor de tus datos**
	  + Uso de **nuevas identidades** con **correos temporales y teléfonos virtuales** temporales
	  + **Confía en las comunidades** afines en vez de en empresas privadas
	  + **Redes sociales libres**
	- ## 2. **Privacidad**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) :
	  ^^**Seguridad Instrumental**^^
	  + Navegadores: [Firefox](https://www.mozilla.org/en-US/firefox/new/), [Ungoogled Chromium](https://github.com/ungoogled-software/ungoogled-chromium), LibreWolf o Icecat
	  + Buscadores: [SearXNG](https://docs.searxng.org/), [DuckDuckGo](https://duckduckgo.com/), [StartPage](https://www.startpage.com/)
	- ## 2. **Privacidad**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) :
	  ^^**Seguridad Instrumental**^^
	  + Extensiones para bloquear publicidad y tracker: [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/), Privacy Badger y [Decentraleyes](https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/?src=collection)
	  + Borrar cookies: [CookieAutoDelete](https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/)
	  + Redirección a frontends libres y abiertos: [LibRedirect](https://libredirect.github.io/) y [UntrackMe](https://f-droid.org/packages/app.fedilab.nitterizeme/) (app móvil)
	  + Bloquear scripts para evitar rastreadores: [NoScript](https://noscript.net/)
	  + Separación de perfiles: [Multi-Account Containers](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/)
	- ## 2. **Privacidad**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) :
	  ^^**Seguridad Instrumental**^^
	  + Utilizar siempre HTTPS, existe la opción nativa en los navegadores y sino con la extensión [HTTPSEverywhere](https://www.eff.org/https-everywhere)
	  + DNS-over-HTTPS (DoH)
	- ## 2. **Privacidad**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) :
	  ^^**Seguridad Instrumental**^^
	  + [[Software Libre en móviles]]
	- ## 2. **Privacidad**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) :
	  ^^**Seguridad Instrumental**^^
	  + Fediverso
	  ![https://nitter.net/pic/media/FiUuL2lXgAAhxc-.jpg%3Fname%3Dlarge](https://nitter.net/pic/media/FiUuL2lXgAAhxc-.jpg%3Fname%3Dlarge){:height 328, :width 567}
	- ## 3. **Redes** ![warning.png](../assets/warning_1686942075028_0.png)
	  + **Nada en la red es anónimo**, al menos hoy en día. Si realmente queremos que nadie sepa lo que hacemos hay que buscar otras formas de navegar internet.
	- ## 3. **Redes**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) 
	  ^^**Seguridad Operacional**^^
	  + No digas nada, no lo relaciones contigo.
	  + Sigue el resto de consejos, especialmente el de separar perfiles.
	- ## 3. **Redes**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) 
	  ^^**Seguridad Instrumental**^^
	  + VPNs: RiseupVPN, WireGuard, TunnerBear
	  + [Tor](https://www.torproject.org/) ([Tails](https://tails.boum.org/), [Whonix](https://www.whonix.org/))
	  + [I2P](https://geti2p.net/en/)
	- ## 4. **Reconocimiento físico** ![warning.png](../assets/warning_1686942075028_0.png)
	  Todas las manifestaciones son grabadas. Las caras y los tatuajes son muy útiles para reconocerte, tápalos.
	  ![](https://podcast.radioalmaina.org/wp-content/uploads/2019/03/cámaras_espiándose.gif){:height 203, :width 344, :align center}
	- ## 4. **Reconocimiento físico**: ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) 
	  ^^**Seguridad Operacional**^^
	  + Tapa todo aquello que te pueda identificar físicamente
	  + Falsea tatuajes o elementos que puedan identificarte
# **Seguridad avanzada**
	- ## 1. **Separación de perfiles** ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) 
	  + Crear distintos perfiles: personal, activista, profesional... con distintos grados de vinculación a ti y totalmente separados entre ellos.
	  + Separarlos desde el principio.
	  + También puede ser útil tener perfiles de usar y tirar.
	- ## 2. **No crear más información de la necesaria** ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) 
	  + Toda información que no generas no puede ser utilizada contra ti
	  + No existe una información mejor protegida que aquélla que no se llega a generar, ya que no existe forma alguna, humana o técnica, de comprometerla
	- ## 3. **Pensamiento ofensivo** ![lightbulb.png](../assets/lightbulb_1686942086742_0.png) 
	  + El atacante utilizará el método que le sea más fácil. Intentar atacarte te ayuda a encontrar los puntos débiles.
	  ![ofensivo.jpg](../assets/ofensivo_1686999242158_0.jpg)
	- ## 4. **OSINT**
	  + [Inteligencia de fuentes abiertas, entrada de Wikipedia](https://es.wikipedia.org/wiki/Inteligencia_de_fuentes_abiertas)
	  + [OSINT framework](https://osintframework.com/)
	  + [OSINT tools collection](https://github.com/cipher387/osint_stuff_tool_collection)
	- ## 5. **Ingeniería social**
	  + Infiltrar, engañar, nada les parece demasiado. Pero nunca parecen así. No hay que caer en la paranoia ni confiarnos demasiado.
	  + Más fácil manipular a las personas que a las máquinas.
	  + Más del 99% de los ataques requieren la intervención humana para conseguir sus objetivos.
	  + Sesgos cognitivos
- # **Conclusiones**
  +  Rutinas donde ir introduciendo nuevos elementos poco a poco
  + Confiar en comunidades y colectivos afines
  + Venirse al hacklab
  + Más info en: [https://reminiscencia.frama.io](https://reminiscencia.frama.io)
- # **Referencias**
	- + [[@RESISTENCIA DIGITAL | MANUAL DE SEGURIDAD OPERACIONAL E INSTRUMENTAL PARA SMARTPHONES]] 
	  + [[Software Libre en móviles]] 
	  + [Guía de Defensa Digital para Organizaciones Sociales](https://lalibre.net/wp-content/uploads/2022/09/Guia-de-proteccion-digital.pdf)
	  + [Manual de autodefensa en la era de la digitalización forzada o Resistencia al capitalismo de vigilancia](https://codeberg.org/PrivacyFirst/Data_Protection/issues)/
- # **Recopilaciones o colecciones de herramientas**
  id:: f4c8bca4-6d5c-4e19-a733-3aa23777b201
	- ## Recopilaciones o colecciones de herramientas
	  + Privacy Guides:
	  	+ [Privacy Guides - Android Overview](https://www.privacyguides.org/os/android-overview/)
	  	+ [Privacy Guides - Android](https://www.privacyguides.org/android/)
	  	+ [Privacy Guides  - Privacy Tools](https://www.privacyguides.org/tools/)
	  + Prism break:
	  	+ [Android](https://prism-break.org/es/categories/android/)
	  	+ [iOS](https://prism-break.org/es/categories/ios/)
	- ## Recopilaciones o colecciones de herramientas
	  + EFF:
	  	+ [Surveillance Self-Defense](https://ssd.eff.org/es)
	  	    + [Android](https://ssd.eff.org/es/search?q=android&commit=Buscar)  
	  	    + [iOS](https://ssd.eff.org/es/search?q=ios&commit=Buscar)  
	  + [The Guardian Project](https://guardianproject.info/)
	  + [Desgooglicemos Internet](https://degooglisons-internet.org/es/)
	  + [switching.software](https://switching.software/)
	- ## Recopilaciones o colecciones de herramientas
	  + [Privacy Tools - Android alternatives](https://www.privacytools.io/android-alternatives)
	  + Gofoss:
	  	+ [Degoogled Phones - Free Your Phone From Google And Apple](https://gofoss.net/intro-free-your-phone/)
	  	+ [Top 75 Tracker Free FOSS Apps (FDroid & Aurora Store)](https://gofoss.net/foss-apps/)
- # Preguntas y debate