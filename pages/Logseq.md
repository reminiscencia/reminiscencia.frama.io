- [web](https://logseq.com/)
- [descargar](https://logseq.com/downloads)
- ## Instalación en beta
	- Descargar el archivo AppImage desde las release y guardarlo en `$HOME/applications/logseq-$LOGSEQ_VERSION.AppImage`
	- Si queremos acceder desde dos usuarios disferentes:
		- Creamos el grupo:
			- ```zsh
			  sudo groupadd logseq
			  ```
		- Añadimos los dos usuarios:
			- ```zsh
			  sudo usermod -aG logseq $USER1
			  sudo usermod -aG logseq $USER2
			  ```
		- Creamos el enlace simbólico:
			- ```zsh
			  sudo ln -s $HOME/applications/logseq-$LOGSEQ_VERSION.AppImage /usr/bin/logseq
			  ```
## Otros enlaces
- [Vídeos oficiales](https://tube.northboot.xyz/c/Logseq)
- [Vídeos de OneStutteringMind](https://inv.vern.cc/channel/UCz7EgrAosr5FRF3IErGV-yQ)
- [Curso de Logseq en castellano](https://invidious.snopyta.org/playlist?list=PLWUX-KZsnKXSi1ynIcrie9Gr48Oaki2W6)